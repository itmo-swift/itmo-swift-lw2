//
//  main.swift
//  lw2
//
//  Created by Daniil Lushnikov on 25.09.2021.
//  Number to string

// a - number for function
let a = Int8(readLine() ?? "") ?? 0
if 0 <= a && a <= 9 {
    print(numberToString(number: a))
}

func numberToString(number: Int8) -> String {
    switch number {
    case 0:
        return "ноль"
    case 1:
        return "один"
    case 2:
        return "два"
    case 3:
        return "три"
    case 4:
        return "четыре"
    case 5:
        return "пять"
    case 6:
        return "шесть"
    case 7:
        return "семь"
    case 8:
        return "восемь"
    case 9:
        return "девять"
    default:
        return "Value should be between 0 and 9 (including)"
    }
}
